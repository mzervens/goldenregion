#include <fstream>
#include "Point.cpp"
#include "GoldenRegion.cpp"
using namespace std;

/**
* @author Mat�ss Z�rv�ns
* @lastModified 01.01.2015
*
* Galven� algoritma implement�cija ir klas� GoldenRegion
* L�nijas tiek padotas ar mas�vu int arr[][]
*
*/
int main()
{
	fstream f;
	int width = 1280;
	int height = 1024;
	const int y = 3;
	GoldenRegion g(width, height);
	
	// Add lines with endpoints greater than screen borders
	int arr[y][4] = { { -10, 1000, 1000, -10 },
					  { 200, -10, 1100, 1034 },
					  {-1, -1, 1280, 1024}
					};

	for (int i = 0; i < y; i++)
	{
		Point p1(arr[i][0], arr[i][1]);
		Point p2(arr[i][2], arr[i][3]);

		g.addLineSegment(p1, p2);
	}

	g.drawLines();
	g.findGoldenRegion();
	g.printPixelMatrix();


	return 0;
}
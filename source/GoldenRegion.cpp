#pragma once
#include "Point.cpp";
#include "LineSegment.cpp";
#include "Pixel.cpp"
#include <iostream>
#include <fstream>
#include <vector>
#include <stack>
#include <map>
#include <limits>
using namespace std;

typedef std::map<pair<int, int>, int> ::iterator mapIt;


/**
 * @author Mat�ss Z�rv�ns
 * @lastModified 01.01.2015
 *
 * Klase realiz� galveno uzdevuma izpildes lo�iku.
 * Klase �auj inicializ�t main�ga izm�ra taisnst�rveida ekr�nu. 
 * Ar metodi addLineSegment ir iesp�jams pievienot k�rt�jo l�niju.
 * Ar metodi drawLines tiek veikta korekto l�niju z�m��ana p�r ekr�na pikse�iem.
 * Ar metodi findGoldenRegion ir iesp�jams mekl�t 'zelta re�ionu'.
 * Ar metodi printPixelMatrix ir iesp�jams izdruk�t ekr�na pikse�us fail�.
 *
 * Detaliz�t�ku meto�u aprakstu skat�t pie attiec�g�s metodes.
 *
 */
class GoldenRegion
{
public:
	int xSize;
	int ySize;
	// Mas�vs glab� visus ekr�na pikse�us
	Pixel ** pixelMatrix;

	// Mas�vs glab� visus korektos nogrie��us, kas ���rso ekr�nu
	vector<LineSegment> lineSegments;

	// V�rdn�ca glab� nep�rkl�jo�os ekr�na poligonus, kurus atdala ekr�nu ���rsojo�ie nogrie��i
	// Poligoni tiek glab�ti k� pikse�a koordin�tas - poligona saturo�o pikse�u skaits
	// Attiec�gi par katru poligonu ir nepiecie�ams zin�t vismaz vienu tam piedero�o pikseli un kop�jo pikse�u skaitu
	map<pair<int, int>, int> regionPixelCount;

	// Glab� 'zelta re�iona' poligonam piedero�a pikse�a koordin�tas
	pair<int, int> goldenRegionPixel;

	// Glab� 'zelta re�iona' poligon� saturo�o pikse�u skaitu
	// Ja pikse�u skaits ir 0, tad tiek uzskat�ts, ka attiec�gaj� uzdevum� nav zelta re�iona
	// (gad�jumi, kad ir vair�ki vien�di minim�lie re�ioni, nav neviena re�iona)
	int goldenRegionSize;

	/**
	 * Inicializ� klasi, tiek inicializ�ti ekr�na pikse�i,
	 * pievienoti nogrie��i, kas atbilst ekr�na mal�m
	 */
	GoldenRegion(int width, int height)
	{
		xSize = width;
		ySize = height;

		pixelMatrix = new Pixel*[width];
		for (int i = 0; i < width; i++)
		{
			pixelMatrix[i] = new Pixel[height];
		}

		initLineSegments();

	}
	~GoldenRegion()
	{
		for (int i = 0; i < xSize; i++)
		{
			delete pixelMatrix[i];
		}
		pixelMatrix = 0;
	}

	/**
	 * Metode �auj pievienot ekr�na ���rsojo�u l�niju.
	 * Ja l�nija piln�b� ne���rso ekr�nu, tad t� netiek pievienota, korektas l�nijas 
	 * gad�jum�, tiek pievienots ��s l�nijas nogrieznis, kura katrs galapunkts ir punkts, kas pieder
	 * pie k�da no ekr�na malas nogrie��a.
	 */
	void addLineSegment(Point p1, Point p2)
	{
		Point * c1 = NULL;
		Point * c2 = NULL;
		Point * tmp = NULL;


		// Check only first 4 line segments which are the monitor border lines
		for (int i = 0; i < 4; i++)
		{
			tmp = findIntersection(lineSegments[i].start, lineSegments[i].end, p1, p2);
			if (tmp != NULL)
			{
				if (c1 == NULL)
				{
					c1 = tmp;
					continue;
				}
				if (c2 == NULL)
				{
					c2 = tmp;
				}
			}
		}

		if (c1 != NULL && c2 != NULL)
		{
			addLineSegmentSafely(c1, c2);
		}

	}

	/**
	 * Metode veic visu korekto nogrie��u z�m��anu p�r ekr�nu.
	 * Metode izmanto Bresenhama l�niju z�m��anas algoritmu
	 * http://en.wikipedia.org/wiki/Bresenham%27s_line_algorithm
	 */
	void drawLines()
	{
		for (int i = 0; i < lineSegments.size(); i++)
		{
			drawLine(lineSegments[i].start.x, lineSegments[i].start.y, lineSegments[i].end.x, lineSegments[i].end.y);
		}

	}

	/**
	 * Metode atrod visus ekr�n� eso�os poligonus, kurus atdala uzz�m�tie nogrie��i.
	 * Metode iet cauri visiem ekr�na pikse�iem, tikl�dz pikselis nav apstr�d�ts, tam izsauc grafa apstaig��anas metodi,
	 * kas sasniedz visus sasniedzamos pikse�us no attiec�g� pikse�a. Katra poligona piedero�ais
	 * pikselis un pikse�u izm�rs attiec�gi tiek saglab�ts v�rdn�c�.
	 * 
	 * P�c visu poligonu atra�anas, tiek v�lreiz apstaig�ts poligons, kas ir 'zelta re�ions'. Katram zelta re�ionam piedero�ajam
	 * pikselim tiek uzst�d�ta paz�me, ka tas pieder pie zelta re�iona.
	 */
	void findGoldenRegion()
	{
		int pixelSize = 0;

		for (int i = 1; i < ySize - 1; i++)
		{
			for (int n = 1; n < xSize - 1; n++)
			{
				if (!(pixelMatrix[n][i].visited) && pixelMatrix[n][i].color != 1)
				{
					pixelSize = traverseGraph(n, i);
					regionPixelCount[make_pair(n, i)] = pixelSize;
				}

			}
		}

		setGoldenRegionVars();
		fillGoldenRegion();

	}

	/**
	 * Metode izdruk� fail� ekr�na pikse�us
	 * Ar simbolu '*' tiek apz�m�ts melnas kr�sas pikselis (pikselis, kas pieder l�nijas segmentam).
	 * Ar simbolu ' ' tiek apz�mets baltas kr�sas pikselis (pikselis, kas pieder k�dam no poligoniem)
	 * Ar simbolu '-' tiek apz�mets zelta re�ionam piedero�s pikselis
	 */
	void printPixelMatrix()
	{
		fstream f;
		f.open("pixels.txt", ios::out);

		for (int i = 0; i < ySize; i++)
		{
			for (int n = 0; n < xSize; n++)
			{
				if (pixelMatrix[n][i].color == 1)
				{
					f << '*';
				}
				else if (pixelMatrix[n][i].goldenRegion == 1)
				{
					f << '-';
				}
				else
				{
					f << ' ';
				}
			}
			f << "\n";
		}
		f << goldenRegionSize;
		f.close();
	}

	void printLineSegments()
	{
		for (int i = 0; i < lineSegments.size(); i++)
		{
			cout << "Line segment (" << lineSegments[i].start.x << " " << lineSegments[i].start.y << ") "
				<< "(" << lineSegments[i].end.x << " " << lineSegments[i].end.y << ") " << "\n \n";

		}


	}


	void printRegions()
	{
		for (mapIt iterator = regionPixelCount.begin(); iterator != regionPixelCount.end(); iterator++)
		{
			cout << "Region with pixel (" << iterator->first.first << " " << iterator->first.second << ") "
				<< "Size: " << iterator->second << "\n \n";
		}

	}

private:

	/**
	 * Sets the golden region variables if the golden region exists
	 */
	void setGoldenRegionVars()
	{
		pair<int, int> minPixelPos;
		int minPixelCount = INT_MAX;
		int dupes = 0;

		for (mapIt iterator = regionPixelCount.begin(); iterator != regionPixelCount.end(); iterator++)
		{
			if (iterator->second < minPixelCount)
			{
				minPixelCount = iterator->second;
				minPixelPos = make_pair(iterator->first.first, iterator->first.second);
				dupes = 0;
				continue;
			}
			else if (iterator->second == minPixelCount)
			{
				dupes = 1;
			}
		}

		if (!(dupes))
		{
			goldenRegionSize = minPixelCount;
			goldenRegionPixel = minPixelPos;
		}
		else
		{
			goldenRegionSize = 0;
		}

	}

	/**
	 * Traverses the golden region polygon, sets each corresponding golden region pixels value
	 * pixel.goldenRegion = true
	 */
	void fillGoldenRegion()
	{
		if (goldenRegionSize <= 0)
		{
			return;
		}

		resetPixelVisits();

		stack<pair<int, int>> st;
		pair<int, int>  cur;

		cur = make_pair(goldenRegionPixel.first, goldenRegionPixel.second);

		while (true)
		{
			if (cur.first < xSize && cur.first >= 0 && cur.second < ySize && cur.second >= 0)
			{
				if (pixelMatrix[cur.first][cur.second].color == 0 && pixelMatrix[cur.first][cur.second].visited == 0)
				{
					pixelMatrix[cur.first][cur.second].goldenRegion = true;
					pixelMatrix[cur.first][cur.second].visited = true;
				}
				else
				{
					if (st.size() > 0)
					{
						cur = st.top();
						st.pop();
					}
					else
					{
						break;
					}
					continue;
				}
			}
			else
			{
				if (st.size() > 0)
				{
					cur = st.top();
					st.pop();
				}
				else
				{
					break;
				}
				continue;
			}

			//These are the regular horizontal and vertical moves
			st.push(make_pair(cur.first + 1, cur.second));
			st.push(make_pair(cur.first - 1, cur.second));
			st.push(make_pair(cur.first, cur.second + 1));
			st.push(make_pair(cur.first, cur.second - 1));


			// These are diagonal moves
			if (cur.first + 1 < xSize && cur.second + 1 < ySize)
			{
				if (isEnclosed(cur.first + 1, cur.second + 1))
				{
					st.push(make_pair(cur.first + 1, cur.second + 1));
				}
			}
			if (cur.first + 1 < xSize && cur.second - 1 < ySize)
			{
				if (isEnclosed(cur.first + 1, cur.second - 1))
				{
					st.push(make_pair(cur.first + 1, cur.second - 1));
				}
			}
			if (cur.first - 1 < xSize && cur.second + 1 < ySize)
			{
				if (isEnclosed(cur.first - 1, cur.second + 1))
				{
					st.push(make_pair(cur.first - 1, cur.second + 1));
				}
			}
			if (cur.first - 1 < xSize && cur.second - 1 < ySize)
			{
				if (isEnclosed(cur.first - 1, cur.second - 1))
				{
					st.push(make_pair(cur.first - 1, cur.second - 1));
				}
			}
			

			if (st.size() > 0)
			{
				cur = st.top();
				st.pop();
			}
			else
			{
				break;
			}
		}

		return;
	}

	/**
	 * A DFS algorithm implementation which traverses all white pixels from starting 
	 * pixel that is white.
	 *
	 * The method outputs traversed vertex count
	 */
	int traverseGraph(int x, int y)
	{
		int count = 0;
		stack<pair<int, int>> st;
		pair<int, int>  cur;

		cur = make_pair(x, y);

		while (true)
		{
			if (cur.first < xSize && cur.first >= 0 && cur.second < ySize && cur.second >= 0)
			{
				if (pixelMatrix[cur.first][cur.second].color == 0 && pixelMatrix[cur.first][cur.second].visited == 0)
				{
					count++;
					pixelMatrix[cur.first][cur.second].visited = true;
				}
				else
				{
					if (st.size() > 0)
					{
						cur = st.top();
						st.pop();
					}
					else
					{
						break;
					}
					continue;
				}
			}
			else
			{
				if (st.size() > 0)
				{
					cur = st.top();
					st.pop();
				}
				else
				{
					break;
				}
				continue;
			}

			// These are regular horizontal and vertical moves
			st.push(make_pair(cur.first + 1, cur.second));
			st.push(make_pair(cur.first - 1, cur.second));
			st.push(make_pair(cur.first, cur.second + 1));
			st.push(make_pair(cur.first, cur.second - 1));

			// These are diagonal moves
			if (cur.first + 1 < xSize && cur.second + 1 < ySize)
			{
				if (isEnclosed(cur.first + 1, cur.second + 1))
				{
					st.push(make_pair(cur.first + 1, cur.second + 1));
				}
			}
			if (cur.first + 1 < xSize && cur.second - 1 < ySize)
			{
				if (isEnclosed(cur.first + 1, cur.second - 1))
				{
					st.push(make_pair(cur.first + 1, cur.second - 1));
				}
			}
			if (cur.first - 1 < xSize && cur.second + 1 < ySize)
			{
				if (isEnclosed(cur.first - 1, cur.second + 1))
				{
					st.push(make_pair(cur.first - 1, cur.second + 1));
				}
			}
			if (cur.first - 1 < xSize && cur.second - 1 < ySize)
			{
				if (isEnclosed(cur.first - 1, cur.second - 1))
				{
					st.push(make_pair(cur.first - 1, cur.second - 1));
				}
			}


			if (st.size() > 0)
			{
				cur = st.top();
				st.pop();
			}
			else
			{
				break;
			}
		}
		
		return count;
	}

	/**
	 * Checks if the pixel is enclosed (if all horizontal and vertical pixels next to it are 
	 * non traversable i.e black)
	 */
	bool isEnclosed(int x, int y)
	{
		bool ok = true;

		if (x + 1 < xSize && x + 1 >= 0 && y >= 0 && y < ySize)
		{
			if (pixelMatrix[x + 1][y].color == 0)
			{
				return false;
			}
		}
		if (x - 1 < xSize && x - 1 >= 0 && y >= 0 && y < ySize)
		{
			if (pixelMatrix[x - 1][y].color == 0)
			{
				return false;
			}
		}
		if (x < xSize && x >= 0 && y + 1 >= 0 && y + 1 < ySize)
		{
			if (pixelMatrix[x][y + 1].color == 0)
			{
				return false;
			}
		}
		if (x < xSize && x >= 0 && y - 1 >= 0 && y - 1 < ySize)
		{
			if (pixelMatrix[x][y - 1].color == 0)
			{
				return false;
			}
		}

		return ok;
	}

	/**
	 * Adds line segment to the lineSegment array
	 * The method will add only if the lineSegment is unique (i.e if there are no duplicates)
	 */
	bool addLineSegmentSafely(Point * s, Point * e)
	{
		for (int i = 0; i < lineSegments.size(); i++)
		{
			if (lineSegments[i].start.x == s->x && lineSegments[i].start.y == s->y
				&& lineSegments[i].end.x == e->x && lineSegments[i].end.y == e->y)
			{
				return false;
			}
			else if (lineSegments[i].start.x == e->x && lineSegments[i].start.y == e->y
				&& lineSegments[i].end.x == s->x && lineSegments[i].end.y == s->y)
			{
				return false;
			}

		}

		lineSegments.push_back(LineSegment(s, e));
		return true;
	}

	/**
	 * Adds initial line segments (screen borders)
	 */
	void initLineSegments()
	{
		Point topLeft(0, 0);
		Point topRight(xSize - 1, 0);
		Point botLeft(0, ySize - 1);
		Point botRight(xSize - 1, ySize - 1);

		lineSegments.push_back(LineSegment(topLeft, topRight));
		lineSegments.push_back(LineSegment(topLeft, botLeft));
		lineSegments.push_back(LineSegment(botRight, topRight));
		lineSegments.push_back(LineSegment(botRight, botLeft));

	}

	/**
	 * Finds if two line segments intersect
	 * The algorithm is implemented using this description:
	 * http://en.wikipedia.org/wiki/Line%E2%80%93line_intersection
	 */
	Point * findIntersection(Point p1, Point p2, Point p3, Point p4)
	{
		bool valid = false;
		float d = (p1.x - p2.x) * (p3.y - p4.y) - (p1.y - p2.y) * (p3.x - p4.x);

		if (d == 0)
		{
			return NULL;
		}

		Point * p = new Point;
		float a = (p1.x * p2.y - p1.y * p2.x) * (p3.x - p4.x) - (p1.x - p2.x) * (p3.x * p4.y - p3.y * p4.x);
		float b = (p1.x * p2.y - p1.y * p2.x) * (p3.y - p4.y) - (p1.y - p2.y) * (p3.x * p4.y - p3.y * p4.x);

		p->x = a / d;
		p->y = b / d;

		if (p1.x < p2.x && p->x >= p1.x && p->x <= p2.x)
		{
			valid = true;
		}
		else if (p1.x > p2.x && p->x <= p1.x && p->x >= p2.x)
		{
			valid = true;
		}

		if (p1.y < p2.y && p->y >= p1.y && p->y <= p2.y)
		{
			valid = true;
		}
		else if (p1.y > p2.y && p->y <= p1.y && p->y >= p2.y)
		{
			valid = true;
		}

		if (valid)
		{

			if (p->x == -0.0)
			{
				p->x = 0.0;
			}
			if (p->y == -0.0)
			{
				p->y = 0.0;
			}

			return p;
		}

		return NULL;
	}


	/**
	 * This is Bresenham's line drawing algorithm
	 * The implementation is take word for word from here:
	 * http://rosettacode.org/wiki/Bitmap/Bresenham%27s_line_algorithm#C.2B.2B
	 */
	void drawLine(float x1, float y1, float x2, float y2)
	{
		const bool steep = (fabs(y2 - y1) > fabs(x2 - x1));
		if (steep)
		{
			std::swap(x1, y1);
			std::swap(x2, y2);
		}

		if (x1 > x2)
		{
			std::swap(x1, x2);
			std::swap(y1, y2);
		}

		const float dx = x2 - x1;
		const float dy = fabs(y2 - y1);

		float error = dx / 2.0f;
		const int ystep = (y1 < y2) ? 1 : -1;
		int y = (int)y1;

		const int maxX = (int)x2;

		for (int x = (int)x1; x<maxX; x++)
		{
			if (steep)
			{
				setBlackPixel(y, x);
			}
			else
			{
				setBlackPixel(x, y);
			}

			error -= dy;
			if (error < 0)
			{
				y += ystep;
				error += dx;
			}
		}
	}

	/**
	 * Sets the pixels color to black
	 */
	void setBlackPixel(int x, int y)
	{
		pixelMatrix[x][y].color = 1;
	}

	/**
	 * Resets the pixel visited signs for all screen pixels
	 * Use this for repeated graph traversals
	 */
	void resetPixelVisits()
	{
		for (int i = 0; i < ySize; i++)
		{
			for (int n = 0; n < xSize; n++)
			{
				pixelMatrix[n][i].visited = 0;
			}
		}

	}


};
#pragma once
#include "Point.cpp"

using namespace std;

/**
* @author Mat�ss Z�rv�ns
* @lastModified 01.01.2015
*
* Klase paredz�ta l�nijas segmentu glab��anai
*
*/
class LineSegment
{
public:
	Point start;
	Point end;

	LineSegment()
	{

	}
	LineSegment(Point p1, Point p2)
	{
		start = p1;
		end = p2;
	}
	LineSegment(Point * p1, Point * p2)
	{
		start.x = p1->x;
		start.y = p1->y;

		end.x = p2->x;
		end.y = p2->y;
	}

};
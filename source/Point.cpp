#pragma once
using namespace std;

/**
* @author Mat�ss Z�rv�ns
* @lastModified 01.01.2015
*
* Klasiska punktu klase
*
*/
class Point
{
public:
	float x;
	float y;
	Point()
	{

	}
	Point(float x, float y)
	{
		this->x = x;
		this->y = y;
	}

};
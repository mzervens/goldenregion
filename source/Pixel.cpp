using namespace std;

/**
* @author Mat�ss Z�rv�ns
* @lastModified 01.01.2015
*
* Ekr�na pikse�u klase
*
*/
class Pixel
{
public:
	bool color;
	bool visited;
	bool goldenRegion;
	Pixel()
	{
		color = false;
		visited = false;
		goldenRegion = false;
		
	}
};